package com.juan.test.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public abstract class CardBase implements Card {

  String number = null;
  CardHolder cardHolder = null;
  Date dueDate = null;
  brandCard brand = null;

  public CardBase(String number, CardHolder cardHolder, Date dueDate, brandCard brand) {
    this.number = number;
    this.cardHolder = cardHolder;
    Calendar cal = Calendar.getInstance();
    cal.setTime(dueDate);
    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
    this.dueDate = cal.getTime();
    this.brand = brand;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public CardHolder getCardHolder() {
    return cardHolder;
  }

  public void setCardHolder(CardHolder cardHolder) {
    this.cardHolder = cardHolder;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  public brandCard getBrand() {
    return brand;
  }

  public void setBrand(brandCard brand) {
    this.brand = brand;
  }

  @Override
  public boolean isValidOperation(BigDecimal amount) {
    return amount.compareTo(new BigDecimal(1000)) < 0;
  }

  @Override
  public boolean isValidCard() {
    return this.dueDate.after(new Date());
  }

  @Override
  public String cardInformation() {
    StringBuilder sb = new StringBuilder();
    sb.append("number: ").append(this.number).append("\n");
    sb.append("name: ").append(this.cardHolder.getName()).append("\n");
    sb.append("lastname: ").append(this.cardHolder.getLastname()).append("\n");
    Calendar cal = Calendar.getInstance();
    cal.setTime(this.dueDate);
    String month = String.valueOf(cal.get(Calendar.MONTH));
    String year = String.valueOf(cal.get(Calendar.YEAR));
    sb.append("duedate: ").append(month).append("/").append(year.substring(2)).append("\n");
    sb.append("brand: ").append(this.brand);
    return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null) return false;
    CardBase cardBase = (CardBase) o;
    return Objects.equals(number, cardBase.number);
  }

  @Override
  public BigDecimal getRateByOperation(BigDecimal amount) {
    if (BigDecimal.ZERO.compareTo(amount) == 0) {
      throw new RuntimeException("The amount can't be zero");
    }
    if (!isValidCard() || !isValidOperation(amount)){
      throw new RuntimeException("Not is possible this operation");
    }
    return (getRateCard().multiply(amount)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
  }
}

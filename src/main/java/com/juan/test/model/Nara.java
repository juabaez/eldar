package com.juan.test.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

public class Nara extends CardBase {

  public Nara() {
    super("", new CardHolder(), new Date(), Card.brandCard.NARA);
  }

  public Nara(String number, CardHolder cardHolder, Date dueDate) {
    super(number, cardHolder, dueDate, Card.brandCard.NARA);
  }

  @Override
  public BigDecimal getRateCard() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(dueDate);
    BigDecimal month = new BigDecimal(cal.get(Calendar.MONTH));
    return month.multiply(BigDecimal.valueOf(0.5));
  }
}

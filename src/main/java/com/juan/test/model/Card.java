package com.juan.test.model;

import java.math.BigDecimal;


public interface Card {
  public enum brandCard {NARA, VISA, AMEX}

  public String cardInformation();

  public boolean isValidOperation(BigDecimal amount);

  public boolean isValidCard();

  BigDecimal getRateCard();

  public BigDecimal getRateByOperation(BigDecimal amount);

  @Override
  public boolean equals(Object o);
}

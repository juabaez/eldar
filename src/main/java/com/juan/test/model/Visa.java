package com.juan.test.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;

public class Visa extends CardBase {

  public Visa() {
    super("", new CardHolder(), new Date(), Card.brandCard.VISA);
  }

  public Visa(String number, CardHolder cardHolder, Date dueDate) {
    super(number, cardHolder, dueDate, Card.brandCard.VISA);
  }

  @Override
  public BigDecimal getRateCard() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(dueDate);
    BigDecimal month = new BigDecimal(cal.get(Calendar.MONTH));
    BigDecimal year = new BigDecimal(String.valueOf(cal.get(Calendar.YEAR)).substring(2));
    if (month.compareTo(BigDecimal.ZERO) == 0) {
      throw new RuntimeException("Month can't be zero");
    }
    return year.divide(month, 2, RoundingMode.HALF_UP);
  }
}

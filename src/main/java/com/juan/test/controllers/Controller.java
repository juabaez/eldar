package com.juan.test.controllers;

import static com.juan.test.model.Card.brandCard.AMEX;
import static com.juan.test.model.Card.brandCard.NARA;
import static com.juan.test.model.Card.brandCard.VISA;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.tomcat.util.buf.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.juan.test.dto.OperationDTO;
import com.juan.test.model.Amex;
import com.juan.test.model.Nara;
import com.juan.test.model.Visa;

@RestController
public class Controller {

  @PostMapping("/rateByOperation")
  public ResponseEntity<HashMap<String, Object>> getRateByOperation(@RequestBody OperationDTO body) {
    HashMap<String, Object> map = new HashMap<>();
    if (body.getBrand() == null || "".compareTo(body.getBrand()) == 0) {
      map.put("brand", "is not present");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
    }
    if (body.getAmount() == null) {
      map.put("amount", "is not present");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
    }

    String brand = body.getBrand();
    BigDecimal amount = body.getAmount();
    try {
      Calendar cal = Calendar.getInstance();
      cal.setTime(new Date());
      cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 5);
      if (VISA.toString().compareTo(brand.toUpperCase()) == 0) {
        Visa visa = new Visa();
        visa.setDueDate(cal.getTime());
        map.put("rateByOperation", visa.getRateByOperation(amount));
      }
      if (AMEX.toString().compareTo(brand.toUpperCase()) == 0) {
        Amex amex = new Amex();
        amex.setDueDate(cal.getTime());
        map.put("rateByOperation", amex.getRateByOperation(amount));
      }
      if (NARA.toString().compareTo(brand.toUpperCase()) == 0) {
        Nara nara = new Nara();
        nara.setDueDate(cal.getTime());
        map.put("rateByOperation", nara.getRateByOperation(amount));
      }
    } catch (RuntimeException e) {
      map.put("Error", e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);
    }
    return ResponseEntity.ok(map);
  }

  @GetMapping("/rate")
  public ResponseEntity<HashMap<String, Object>> getRate(
      @RequestParam(value = "brand", required = true) String brand) {
    HashMap<String, Object> map = new HashMap<>();
    if (VISA.toString().compareTo(brand.toUpperCase()) == 0) {
      map.put("rate", (new Visa()).getRateCard());
    }
    if (AMEX.toString().compareTo(brand.toUpperCase()) == 0) {
      map.put("rate", (new Amex()).getRateCard());
    }
    if (NARA.toString().compareTo(brand.toUpperCase()) == 0) {
      map.put("rate", (new Nara()).getRateCard());
    }
    return ResponseEntity.ok(map);
  }
}

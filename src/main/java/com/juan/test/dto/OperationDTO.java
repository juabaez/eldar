package com.juan.test.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class OperationDTO {
  private String brand;
  private BigDecimal amount;
}

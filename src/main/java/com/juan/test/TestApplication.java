package com.juan.test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.juan.test.model.Amex;
import com.juan.test.model.Card;
import com.juan.test.model.CardBase;
import com.juan.test.model.CardHolder;
import com.juan.test.model.Nara;
import com.juan.test.model.Visa;

@SpringBootApplication
public class TestApplication {

  public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(TestApplication.class, args);

    CardHolder cardHolder = new CardHolder();
    cardHolder.setName("juan");
    cardHolder.setLastname("baez");

    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.MONTH, 8);
    cal.set(Calendar.YEAR, 2025);

    Visa visa = new Visa("123456718910", cardHolder, cal.getTime());

    Amex amex = new Amex("123456561910", cardHolder, cal.getTime());

    Nara nara = new Nara("123456718910", cardHolder, cal.getTime());

    System.out.println(visa.cardInformation());
    System.out.println("Valid operation: " + visa.isValidOperation(new BigDecimal(1000)));
    System.out.println("Valid operation: " + visa.isValidOperation(new BigDecimal(500)));
    System.out.println("Valid for operate " + visa.isValidCard());
    System.out.println("Is same card Nara "+visa.equals(nara));
    System.out.println("Is same card Amex "+visa.equals(amex));
    System.out.println("rate " + visa.getRateCard());
    System.out.println("rate by operation " + visa.getRateByOperation(new BigDecimal(600)));

  }

}
